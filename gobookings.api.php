<?php

/**
 * @file
 * GoBookings Documentation and API example.
 */

/**
 * @defgroup gobookings_api GoBookings
 * @{
 * API for user accounts, create profile, fetch profile, and fetch bookings.
 *
 * @section fetch profile
 *
 * Example:
 * @code
 *
 * // Get gobookings.handler service.
 * $gobookings_service = \Drupal::service('gobookings.handler');
 * // Fetch data
 * $gobooking_fetched_profile = $gobookings_service->getCustomersProfile($calendar_oid, $user_client_id);
 * @endcode
 *
 *
 * @section fetch bookings
 *
 * Example:
 * @code
 *
 * // Get gobookings.handler service.
 * $gobookings_service = \Drupal::service('gobookings.handler');
 * // Fetch data
 * $bookings = $gobookings_service->searchCustomerBooking($calendar_oid, $user_client_id);
 * @endcode
 *
 * @}
 */

/**
 * @} End of "defgroup gobookings_api".
 */
