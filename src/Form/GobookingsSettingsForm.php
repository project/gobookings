<?php

namespace Drupal\gobookings\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\gobookings\Service\GobookingsService;

/**
 * Class GobookingsSettingsForm.
 *
 * @package Drupal\gobookings\Form
 */
class GobookingsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'gobookings.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gobookings_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('gobookings.settings');
    // Sign in form settings.
    $form['details'] = [
      '#type' => 'details',
      '#title' => $this->t('Go Bookings settings'),
      '#open' => TRUE,
    ];

    $form['details']['user_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User ID'),
      '#default_value' => $config->get('user_id'),
      '#required' => TRUE,
    ];

    $form['details']['business_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Business Name'),
      '#default_value' => $config->get('business_name'),
    ];

    $form['details']['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
    ];

    $form['wsdl_location'] = [
      '#type' => 'select',
      '#title' => $this->t('Where to retrieve the wsdl from'),
      '#description' => $this->t('Retrieving the wsdl from Go Bookings repeatedly may not be desired, using a local file should be faster.'),
      '#options' => [
        'local' => 'Bundled local wsdl',
        'remote' => 'Fetch from GoBookings',
      ],
      '#default_value' => $config->get('wsdl_location')
    ];

    $form['error_logging'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Error logging'),
      '#description' => $this->t('If enabled, errors are logged to drupal logger service (Recommended)'),
      '#default_value' => $config->get('error_logging'),
    ];

    $form['extended_logging'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Extended logging'),
      '#description' => $this->t('If enabled, every info and noticed also is logged to drupal logger service'),
      '#default_value' => $config->get('extended_logging'),
    ];

    $form['retries'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Retries'),
      '#description' => $this->t('Number of times the service will retry on failure, can be 0'),
      '#size' => 1,
      '#default_value' => $config->get('retries'),
    ];
    $current_password = $config->get('password');
    if (!empty($current_password)) {
      $form['details']['password']['#description'] = $this->t('A password required by the Gobookings server. <em>The currently set password is hidden for security reasons</em>.');

      $form['details']['delete_password'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Delete the stored password'),
      ];
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('gobookings.settings');
    $settings_values = $form_state->cleanValues()->getValues();

    if (empty($settings_values['password'])) {
      $settings_values['password'] = $config->get('password');
    }
    if (!empty($settings_values['delete_password'])) {
      $settings_values['password'] = '';
    }
    // Dont need to save this checkbox.
    unset($settings_values['delete_password']);
    // Save values to config.
    $config->setData($settings_values)->save();
  }

}
