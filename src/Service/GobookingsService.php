<?php

namespace Drupal\gobookings\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\Client;
use Meng\AsyncSoap\Guzzle\Factory;

/**
 * Gobookings service.
 *
 * @package Drupal\gobookings\Service
 */
class GobookingsService implements GobookingsInterface {

  use StringTranslationTrait;

  /**
   * Web Service paths and version.
   */
  const GB_WSDL = 'https://www.gobookings.com/webservices/v2/Service.asmx?WSDL';

  /**
   * Web Service paths and version.
   */
  const GB_WSDL_FILE = 'file://' . __DIR__ . '/../../gobookings.wsdl';

  /**
   * Web Service base path.
   */
  const GB_QUERY_BASE_PATH = 'https://www.gobookings.com/webservices/v2/';

  /**
   * Status constants.
   */
  const GB_STATUS_SUCCESS = 'SUCCESS';

  const GB_STATUS_ERROR = 'Error';

  /**
   * GoBooking response error string constants.
   */
  const GB_ERROR = 'Error';

  const SUPPRESS_ERROR = TRUE;

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Soap Client.
   *
   * @var \Meng\AsyncSoap\Guzzle\Factory
   */
  protected $client;

  /**
   * Logger service for gobookings.
   *
   * @var \Drupal\gobookings\Service\GobookingsLogger
   */
  protected $logger;

  /**
   * Number of times to retry.
   *
   * @var int
   */
  protected $retries;

  /**
   * Loaded gobookings settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, GobookingsLogger $logger) {
    $this->configFactory = $config_factory;
    $this->logger = $logger;
    $this->config = $this->configFactory->get('gobookings.settings');
    $this->retries = $this->config->get('retries');
    $factory = new Factory();

    if ($this->config->get('wsdl_location') === 'remote') {
      $wsdl_location = self::GB_WSDL;
    }
    else {
      $wsdl_location = self::GB_WSDL_FILE;
    }
    $this->client = $factory->create(new Client(), $wsdl_location);
  }

  /**
   * {@inheritdoc}
   */
  public function createClientProfile($oid, $profile_user, $profile_password, $profile_email, $first_name, $last_name) {
    $query_arguments = [
      'OID' => $oid,
      'SecurityQuestion' => 'Question' . rand(),
      'SecurityAnswer' => 'no_idea' . rand(),
      'ClientUsername' => $profile_user,
      'ClientPassword' => $profile_password,
      'profile_email' => $profile_email,
      'BusinessName' => $this->config->get('business_name'),
      'NameLabel1' => $first_name,
      'NameLabel2' => $last_name,
      'Email' => $profile_email,
    ];

    $formatted_response = $this->doRequest(ucfirst(__FUNCTION__), $query_arguments);

    if ($formatted_response['status'] === self::GB_STATUS_SUCCESS) {
      $profile = $formatted_response['raw_response'];
      if (property_exists($profile, 'ClientID')) {
        return reset($profile->ClientID);
      }
      $this->logger->logMessage($this->logger::LOG_ERROR, 'Duplicated username in GoBookings, @username: ', ['@username' => $profile_user]);
    }
    $this->logger->logMessage($this->logger::LOG_ERROR, $formatted_response['raw_response']);
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function searchCustomerBooking($oid, $client_id, $suppress_error = self::SUPPRESS_ERROR) {
    $query_arguments = [
      'OID' => $oid,
      'ClientID' => $client_id,
    ];
    $formatted_response = $this->doRequest(ucfirst(__FUNCTION__), $query_arguments);

    if ($formatted_response['status'] === self::GB_STATUS_SUCCESS) {
      $bookings = $formatted_response['raw_response'];
      return (array) $bookings;
    }
    if (!$suppress_error) {
      $this->logger->logMessage($this->logger::LOG_ERROR, $formatted_response['raw_response']);
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCustomersProfile($oid, $client_id) {
    $query_arguments = [
      'OID' => $oid,
      'ClientID' => $client_id,
    ];
    $formatted_response = $this->doRequest(ucfirst(__FUNCTION__), $query_arguments);

    if ($formatted_response['status'] === self::GB_STATUS_SUCCESS) {
      $profile = $formatted_response['raw_response'];
      return (array) $profile;
    }
    $this->logger->logMessage($this->logger::LOG_ERROR, $formatted_response['raw_response']);
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function customerResetPassword($client_id, $profile_password) {
    $query_arguments = [
      'client_id' => $client_id,
      'profile_password' => $profile_password,
    ];

    $formatted_response = $this->doRequest(ucfirst(__FUNCTION__), $query_arguments);

    if ($formatted_response['status'] === self::GB_STATUS_SUCCESS) {
      return $formatted_response['raw_response'];
    }
    $this->logger->logMessage($this->logger::LOG_ERROR, $formatted_response['raw_response']);
    return FALSE;
  }

  /**
   * Handle a client request.
   *
   * @param string $method
   *   Method to call on webservice request.
   * @param array $query_arguments
   *   Array of parameters to pass on.
   *
   * @return array
   *   Returns a response array.
   *
   * @see formatResponse
   */
  public function doRequest($method, array $query_arguments) {
    $this->logger->logMessage($this->logger::LOG_OTHER, '@method method is called.', ['@method' => $method]);
    $query_arguments = array_merge($query_arguments, $this->getRequestDefaults());
    $log_arguments = $query_arguments;
    if (isset($query_arguments['Password'])) {
      $log_arguments['Password'] = str_pad(substr($log_arguments['Password'], 0, 4), strlen($log_arguments['Password']), 'X') . ' -- Redacted --';
    }
    $this->logger->logMessage($this->logger::LOG_OTHER, 'Sent variables to GoBookings webservice: @variables', ['@variables' => print_r($log_arguments, TRUE)]);
    return $this->queryGoBookings($method, $query_arguments);
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestDefaults() {
    return [
      'UserID' => $this->config->get('user_id'),
      'Password' => $this->config->get('password'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function queryGoBookings($method, array $query_arguments) {
    for ($i = 0; $i <= $this->retries; $i++) {
      // Call method on webservice.
      $promise = $this->client->$method($query_arguments);
      $response = $promise->wait();
      $result = end($response);
      if (is_object($result) && !empty($result)) {
        // Success, log and exit.
        $this->logger->logMessage($this->logger::LOG_OTHER, 'Request success on try @try_number', ['@try_number' => $i + 1]);
        $this->logger->logMessage($this->logger::LOG_OTHER, "Go Booking response:\n" . print_r((array) $response, TRUE));
        return $this->validateAndSanitiseWebserviceResponse($result);
      }
      else {
        $this->logger->logMessage($this->logger::LOG_OTHER, 'Error on try @try_number', ['@try_number' => $i + 1]);
      }
    }
    return $this->formatResponse(self::GB_STATUS_ERROR, 'Failed connect to Gobookings');
  }

  /**
   * Return a response array for consumption.
   *
   * The response array has the following values:
   * 'status' - the gobookings service status ie. 'Success', 'Error', 'Unknown'
   * 'raw_response' - the raw response returned from the request.
   *
   * @param string $status
   *   The gobookings service status.
   * @param mixed $raw_response
   *   The response from webservice call.
   *
   * @return array
   *   The response array, as outlined above.
   */
  protected function formatResponse($status, $raw_response = '') {
    $return_array = [
      'status' => $status,
      'raw_response' => $raw_response,
    ];

    return $return_array;
  }

  /**
   * {@inheritdoc}
   */
  public function validateAndSanitiseWebserviceResponse(\stdClass $webservice_response) {
    $result = simplexml_load_string($webservice_response->any);
    if (empty($result)) {
      return $this->formatResponse(self::GB_STATUS_ERROR, 'Empty result received from gobookings.');
    }
    $data_set = $result->xpath('NewDataSet/Return')[0];
    if (empty($data_set) || property_exists($data_set, self::GB_ERROR)) {
      $error = self::GB_ERROR;
      $error_message = $data_set->$error ? end($data_set->$error) : 'Empty result received from gobookings. check sent variable.';
      return $this->formatResponse(self::GB_STATUS_ERROR, $error_message);
    }
    return $this->formatResponse(self::GB_STATUS_SUCCESS, $data_set);
  }

}
