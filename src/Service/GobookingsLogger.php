<?php

namespace Drupal\gobookings\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Gobookings logger.
 *
 * @package Drupal\gobookings\Service
 */
class GobookingsLogger {

  /**
   * Error Logging constants.
   */
  const LOG_ERROR = 'Error';
  const LOG_OTHER = 'OTHER';
  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Logger for a channel.
   *
   * @var \Psr\Log\loggerInterface
   */
  protected $logger;

  /**
   * Whether to record error information.
   *
   * @var bool
   */
  protected $errorLogging;

  /**
   * Whether to record extended logging information.
   *
   * @var bool
   */
  protected $extendedLogging;

  /**
   * Loaded gobookings settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger_factory) {
    $this->configFactory = $config_factory;
    $this->logger = $logger_factory->get('Go Bookings');
    $this->config = $this->configFactory->get('gobookings.settings');
    $this->errorLogging = $this->config->get('error_logging');
    $this->extendedLogging = $this->config->get('extended_logging');
  }

  /**
   * Log a message to the global drupal logger.
   *
   * @param string $type
   *   Severity of error.
   * @param string $message
   *   The message text.
   * @param array $context
   *   Placeholders array to replace in text.
   *
   * @see https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-3-logger-interface.md
   */
  public function logMessage($type, $message, array $context = []) {
    if ($type === self::LOG_ERROR && $this->errorLogging) {
      $this->logger->error($message, $context);
    }
    elseif ($type === self::LOG_OTHER && $this->extendedLogging) {
      $this->logger->notice($message, $context);
    }
  }

}
