<?php

namespace Drupal\gobookings\Service;

/**
 * Service for Gobookings webservice transactions.
 *
 * @group gobookings
 */
interface GobookingsInterface {

  /**
   * Create Client Profile on a single calendar.
   *
   * @param string $oid
   *   The calendar ID in GoBookings.com.
   * @param string $profile_user
   *   Username to register.
   * @param string $profile_password
   *   User's temporary password.
   * @param string $profile_email
   *   User's email.
   * @param string $first_name
   *   Use to set in creating profile.
   * @param string $last_name
   *   Use to set in creating profile.
   *
   * @return string|false
   *   Created client id, false other wise.
   */
  public function createClientProfile($oid, $profile_user, $profile_password, $profile_email, $first_name, $last_name);

  /**
   * SearchCustomerBooking on a single calendar.
   *
   * @param string $oid
   *   The calendar ID in GoBookings.com.
   * @param string $client_id
   *   User's $client_id.
   * @param bool $suppress_error
   *   Whether to log error.
   *
   * @return array|false
   *   Array of booking properties, false otherwise.
   */
  public function searchCustomerBooking($oid, $client_id, $suppress_error);

  /**
   * GetCustomersProfile on a single calendar.
   *
   * @param string $oid
   *   The calendar ID in GoBookings.com.
   * @param string $client_id
   *   User's $client_id.
   *
   * @return string|false
   *   Array of profile properties, false otherwise.
   */
  public function getCustomersProfile($oid, $client_id);

  /**
   * Webservice method to reset password.
   *
   * @param string $client_id
   *   User's Client ID.
   * @param string $profile_password
   *   User's new password.
   *
   * @return null|false
   *   Webservice response object false on fail or not result found.
   */
  public function customerResetPassword($client_id, $profile_password);

  /**
   * Get the default query parameters array.
   *
   * @return array
   *   The default arguments for request.
   */
  public function getRequestDefaults();

  /**
   * Send a request to the middleware.
   *
   * @param string $method
   *   Method to call at webservice request.
   * @param array $query_arguments
   *   Arguments.
   *
   * @return \SimpleXMLElement|false
   *   Response xml.
   */
  public function queryGoBookings($method, array $query_arguments);

  /**
   * Validate and fetch the data out of webservice response.
   *
   * @param object $webservice_response
   *   GoBookings webservice response.
   *
   * @return \SimpleXMLElement|false
   *   Fetched data, false otherwise.
   */
  public function validateAndSanitiseWebserviceResponse(\stdClass $webservice_response);

}
