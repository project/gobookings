[GoBookings] (http://gobookings.com) is a website provides GUI and webservice for online booking.

This module provides integration API for GoBookings XML based WSDL webservice.

Requirements
------------

1. Register an account on GoBookings for calendar administration.
2. Obtain web service userId, password and business name.
3. Create calendar(s).
4. Provide some free future time-slot in created calendar.
5. The simplexml PHP extenstion.
6. meng-tian/php-async-soap library is required and can be installed by using composer require "meng-tian/php-async-soap".

Restrictions
------------
Compatible with GoBookings web service version 1.1.

Documentation and API
---------------------
Take a look at gobookings.api.php for API Documentation and sample.

Install using Composer (recommended)
------------------------------------
If you use Composer to manage dependencies, edit `/composer.json` as follows.

1. Run `composer require --prefer-dist drupal/gobookings`.
